CREATE DATABASE  IF NOT EXISTS `nchl` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `nchl`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: nchl
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cips_account`
--

DROP TABLE IF EXISTS `cips_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cips_account` (
  `identifier` varchar(50) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `device_id` varchar(45) DEFAULT NULL,
  `amount` varchar(15) DEFAULT NULL,
  `debit_type` char(1) DEFAULT NULL,
  `frequency` varchar(2) DEFAULT NULL,
  `mandate_start_date` date DEFAULT NULL,
  `mandate_expiry_date` date DEFAULT NULL,
  `mandate_token` varchar(255) DEFAULT NULL,
  `mandate_token_type` varchar(1) DEFAULT NULL,
  `entry_id` varchar(20) DEFAULT NULL,
  `mandate_token_nickname` varchar(50) DEFAULT NULL,
  `bank_id` varchar(10) DEFAULT NULL,
  `bank_name` varchar(200) DEFAULT NULL,
  `requested_on` datetime DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `cips_account_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cips_account`
--

LOCK TABLES `cips_account` WRITE;
/*!40000 ALTER TABLE `cips_account` DISABLE KEYS */;
INSERT INTO `cips_account` VALUES ('00e861ac-a3b9-47dd-83aa-535e034f723e','9813527106','VIVO V7','1000000.00','V','7','2022-07-25','2023-01-20','OlkEx1wGwp0rEDpeGobr3umi6AwVHJhbqT9TfBwWNKlK8reRtsLQlIHFE3L8OBmK4OQajEIURCAr9N4QY29W60Z+Iv4aL6F5bbBJ/1BDkJ5Y97wWgENPmlw712n7PPW3b2lWhj90MZTPJzU7+KbnARhYTM0KCZpH5JG352K9PP4=','F','343','*********015','2501','NMB Bank Limited','2022-07-25 12:19:07'),('18d5b03e-8e95-4016-b2b4-1cde4596feb7','9841192019','VIVO V7','1000000.00','V','7','2022-07-27','2023-01-22','OlkEx1wGwp0rEDpeGobr3umi6AwVHJhbqT9TfBwWNKlK8reRtsLQlIHFE3L8OBmK4OQajEIURCAr9N4QY29W60Z+Iv4aL6F5bbBJ/1BDkJ5Y97wWgENPmlw712n7PPW3b2lWhj90MZTPJzU7+KbnARhYTM0KCZpH5JG352K9Ps4=',NULL,NULL,NULL,NULL,NULL,'2022-07-27 16:34:03'),('32197bb2-f086-4bdf-9373-e771b6ada722','9813527106','VIVO V7','1000000.00','V','7','2022-08-11','2023-02-06','OlkEx1wGwp0rEDpeGobr3umi6AwVHJhbqT9TfBwWNKm6OgJX/lNWDWKslw4osIk4xcLznOEPpAL+6n/Sy4BWZaLG+SkHERQ2+nCkC8zxz3olUaruqkYvoT+KVWPFBFs8/RSb39jNdsY92nmvUESG/DBrRtA1SJqHE8SCjVSriJ4=','F','345','*********015','2501','NMB Bank Limited','2022-08-10 15:28:51');
/*!40000 ALTER TABLE `cips_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cips_token`
--

DROP TABLE IF EXISTS `cips_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cips_token` (
  `r_token` varchar(100) DEFAULT NULL,
  `r_exp_date` timestamp NULL DEFAULT NULL,
  `a_token` varchar(100) DEFAULT NULL,
  `a_exp_date` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cips_token`
--

LOCK TABLES `cips_token` WRITE;
/*!40000 ALTER TABLE `cips_token` DISABLE KEYS */;
INSERT INTO `cips_token` VALUES ('ZjAzMzBjYWEtOGVkYi00ZWU3LTljNjYtYWQ2MjkyMGNhNTY3','2022-08-11 09:36:28','YmU0ZDUyOGEtN2I1OS00MmNjLWJlYmItMTQ2MWZkYzQzYTYy','2022-08-11 04:48:56','2022-08-11 04:36:29');
/*!40000 ALTER TABLE `cips_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cips_transaction`
--

DROP TABLE IF EXISTS `cips_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cips_transaction` (
  `tid` varchar(20) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `type` varchar(15) DEFAULT NULL,
  `nchl_txn_id` varchar(20) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tid`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `cips_transaction_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cips_transaction`
--

LOCK TABLES `cips_transaction` WRITE;
/*!40000 ALTER TABLE `cips_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `cips_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` varchar(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('9813527106','prajwal','prajwal@moco.com.np'),('9841192019','Pramila','example@moco.com.np'),('9849719431','Vivek','vivek@moco.com.np'),('9849751896','shakya','shakya@moco.com.np');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-11 10:46:10
