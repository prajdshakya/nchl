#@ignore
Feature: Connect Ips Get Account Detail Negative Test Cases

  Background:
    * url baseUrl
    * def xmt = xmocotoken
    * def xmd = xmocodevice

    #NOTE that X-MOCO-AUTH-TOKEN,X-MOCO-DEVICE are not send in header
  Scenario: Get account detail without X-MOCO-AUTH-TOKEN,X-MOCO-DEVICE
    Given path '/account/00e861ac-a3b9-47dd-83aa-535e034f723e'
    When method GET
    And print response
    * def expectedStatus = 400
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_PARAM_MISSING'

    #NOTE identifier must be proper UUID otherwise it should error, here abcd is used as identifier which is not in
      #   proper UUID format
  Scenario: Get account detail with invalid identifier
    Given path '/account/abcd'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 422
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_INVALID_DATA'

    #NOTE That here identifier is in proper format but it doesnt exist in the system
  Scenario: Get account detail with identifier which doesnt exist in system
    Given path '/account/3f4e3f3d-34c9-42af-b2fe-40df3e8a0de9'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 404
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_NOT_FOUND'

    #NOTE That here identifier is in proper format but it doesnt exist in the system
  Scenario: Get account detail with identifier which mandate token has not been updated
    Given path '/account/3f4e3f3d-34c9-42af-b2fe-40df3e8a0de9'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 404
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_NOT_FOUND'
