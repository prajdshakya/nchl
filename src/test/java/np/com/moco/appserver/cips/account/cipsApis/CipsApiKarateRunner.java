package np.com.moco.appserver.cips.account.cipsApis;

import com.intuit.karate.junit5.Karate;

public class CipsApiKarateRunner {
    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }
}
