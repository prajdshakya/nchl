#@ignore
Feature: Connect Ips Get Account Bank Image Negative Test Cases

  Background:
    * url baseUrl
    * def xmt = xmocotoken
    * def xmd = xmocodevice


    #Note that X-MOCO-AUTH-TOKEN,X-MOCO-DEVICE are not set in header
  Scenario: Get bank image without X-MOCO-AUTH-TOKEN,X-MOCO-DEVICE
    Given path '/account/image/2501'
    When method GET
    And print response
    * def expectedStatus = 400
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_PARAM_MISSING'

    #Note that bank id must be 4 or 5 digits only number if less than 4 or more than 5 it should produce error,
    #here only 3 digits are used.
  Scenario: Get bank image of bank id less than 4 and greater than 5
    Given path '/account/image/250'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 422
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_INVALID_DATA'

    #Note that bank id must be 4 or 5 digits only number if less than 4 or more than 5 it should produce error,
    #here characters are used to get image.
  Scenario: Get bank image of bank id as characters
    Given path '/account/image/abcde'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 422
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_INVALID_DATA'

    #Note that image of bank id 40111 does not exists in the folder, so it should return with error
  Scenario: Get bank image which doesnt exists in the folder
    Given path '/account/image/40111'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 404
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_NOT_FOUND'
