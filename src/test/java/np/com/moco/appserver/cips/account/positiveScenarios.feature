#@ignore
Feature: Connect Ips Positive test cases

  Background:
    * url baseUrl
    * def xmt = xmocotoken
    * def xmd = xmocodevice

  Scenario: Get Initial Data for Login form
    Given path '/account'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 200
    And print responseStatus
    Then assert responseStatus == expectedStatus
  
  Scenario: Post mandate token from NCHL
    Given path '/account/mandatetoken'
    And def BodyOfRequest = read('jsons/positive/mandateTokenPostBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/positive/mandateTokenSuccessResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

  Scenario: Get account status
    Given path '/account/status'
    And  param participantId = 'MOCO@999'
    And  param identifier = '00e861ac-a3b9-47dd-83aa-535e034f723e'
    When method GET
    And print response
    * def expectedStatus = 200
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_OK'

  Scenario: Get account detail
    Given path '/account/00e861ac-a3b9-47dd-83aa-535e034f723e'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 200
    And print responseStatus
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/positive/accountDetailSuccessResponse.json')
    And match $ == expectedResponse
    * print expectedResponse
    
  Scenario: Get bank image of given bank id
    Given path '/account/image/2501'
    And header X-MOCO-AUTH-TOKEN = xmt
    And header X-MOCO-DEVICE = xmd
    When method GET
    And print response
    * def expectedStatus = 200
    And print responseStatus
    Then assert responseStatus == expectedStatus



