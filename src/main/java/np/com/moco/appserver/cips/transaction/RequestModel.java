package np.com.moco.appserver.cips.transaction;

import org.json.JSONObject;

public class RequestModel {
    private String url;
    private JSONObject mainJson;

    public RequestModel(String url, JSONObject mainJson) {
        this.url = url;
        this.mainJson = mainJson;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JSONObject getMainJson() {
        return mainJson;
    }

    public void setMainJson(JSONObject mainJson) {
        this.mainJson = mainJson;
    }

    @Override
    public String toString() {
        return "RequestModel{" + "url=" + url + ", mainJson=" + mainJson + '}';
    }
}
