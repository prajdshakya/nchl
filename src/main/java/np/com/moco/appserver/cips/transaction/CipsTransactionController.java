package np.com.moco.appserver.cips.transaction;

import com.google.gson.Gson;
import np.com.moco.appserver.cips.common.Log;
import np.com.moco.appserver.cips.common.TokenGenerator;
import np.com.moco.appserver.cips.common.TokenValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import np.com.moco.appserver.cips.utils.Config;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Random;

public class CipsTransactionController {
    private static final Logger LOG = LogManager.getLogger(CipsTransactionController.class);
    private static Log log;

    public static Object makePaymentRequest(Request request, Response response) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.MAKEPAYMENTREQUEST, "Starting to send payment request");
        LOG.info(log.toString());

        response.type("application/json");

        //CHECKING EMPTY X-MOCO-AUTH-TOKEN, X-MOCO-DEVICE
        String authtoken = request.headers("X-MOCO-AUTH-TOKEN");
        String mocoDevice = request.headers("X-MOCO-DEVICE");
        if ((authtoken==null || authtoken=="") || (mocoDevice==null || mocoDevice=="")){
            response.status(400);
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Missing X-MOCO-AUTH-TOKEN or X-MOCO-DEVICE");
            LOG.error(log.toString());
            return "GNR_PARAM_MISSING";
        }

        //VALIDATING REQUEST BODY NULL/EMPTY
        CipsPaymentRequest cipsPaymentRequest;
        if (request.body().equals("") || request.body().equals(null)){
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Missing JSON body");
            LOG.error(log.toString());
            response.status(400);
            return "GNR_PARAM_MISSING";
        }
        //MAPPING REQUEST BODY TO CipsPaymentRequest
        cipsPaymentRequest = new Gson().fromJson(new JSONObject(request.body()).toString(), CipsPaymentRequest.class);


        //CHECKING NULL/EMPTY VALUE
        if ((cipsPaymentRequest.getMerchant()==null || cipsPaymentRequest.getTxn()==null) ||
                (cipsPaymentRequest.getMerchant().getMid()==null || cipsPaymentRequest.getMerchant().getName()==null) ||
                (cipsPaymentRequest.getTxn().getType()==null)){
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Error while validating data");
            LOG.error(log.toString());
            response.status(422);
            return "GNR_INVALID_DATA";
        }
        if ((cipsPaymentRequest.getMerchant().equals("") || cipsPaymentRequest.getTxn().equals("")) ||
                (cipsPaymentRequest.getMerchant().getMid().equals("") || cipsPaymentRequest.getMerchant().getName().equals("")) ||
                (cipsPaymentRequest.getTxn().getType().equals(""))){
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Error while validating data");
            LOG.error(log.toString());
            response.status(400);
            return "GNR_PARAM_MISSING";
        }

        //CREATED TO COLLECT DATA TO SAVE TRANSACTION
        JSONObject saveTransactionJson= new JSONObject();

        //TODO: need to validate mPin  (is it required only for requestPayment || do we need it in cancel as well)

//------------------------------------------------------------------------------------------------------------------------
        //FOR PAYMENT i.e. if txn type in request body is sale
//------------------------------------------------------------------------------------------------------------------------
        if (cipsPaymentRequest.getTxn().getType().equals("Sale")){

            if ((cipsPaymentRequest.getCips()==null) ||
                    (cipsPaymentRequest.getCips().getMandateToken()==null || cipsPaymentRequest.getCips().getmPin()==null || cipsPaymentRequest.getCips().getMandateExpiryDate()==null) ||
                    (cipsPaymentRequest.getTxn().getAmount()==null) || cipsPaymentRequest.getTxn().getRemarks()==null){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Error while validating data");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }
            if ((cipsPaymentRequest.getCips().equals("")) ||
                    (cipsPaymentRequest.getCips().getMandateToken().equals("") || cipsPaymentRequest.getCips().getmPin().equals("") || cipsPaymentRequest.getCips().getMandateExpiryDate().equals("")) ||
                    (cipsPaymentRequest.getTxn().getAmount().equals("")) || cipsPaymentRequest.getTxn().getRemarks().equals("")){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Error while validating data");
                LOG.error(log.toString());
                response.status(400);
                return "GNR_PARAM_MISSING";
            }
            //PRINTING REQUEST BODY TO CHECK ERRORS IN FUTURE
            LOG.info("Request body : "+cipsPaymentRequest.toString());

            //SEARCH DATA FROM MANDATE TOKEN
            Map<String, String> userByMandateToken = CipsTransactionDao.getByMandateToken(cipsPaymentRequest.getCips().getMandateToken());
            if (userByMandateToken.get("userIdentifier")==null || userByMandateToken.get("userIdentifier").equals("")){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Mandate token does not exists in system");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }

            //COMPARING AMOUNT IN PAYLOAD WITH AMOUNT IN CONFIG
            double amountInPayload = Double.parseDouble(cipsPaymentRequest.getTxn().getAmount());
            double amountInConfig = Double.parseDouble(Config.getProperty("cips_amount"));
            if (amountInPayload>amountInConfig){
                log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Amount in payload is greater than 'cips_amount' set in config");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }

            //COLLECTING DATA TO SAVE TRANSACTION
            saveTransactionJson.put("userIdentifier", userByMandateToken.get("userIdentifier"));
            saveTransactionJson.put("amount", cipsPaymentRequest.getTxn().getAmount());
            saveTransactionJson.put("mid", cipsPaymentRequest.getMerchant().getMid());
            saveTransactionJson.put("name", cipsPaymentRequest.getMerchant().getName());
            saveTransactionJson.put("type", cipsPaymentRequest.getTxn().getType());

            //CHECKING MANDATE TOKEN VALIDITY TIME PERIOD
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime now = LocalDateTime.now();
            String dateAndTimeNow=dtf.format(now);
            if (dateAndTimeNow.compareTo(cipsPaymentRequest.getCips().getMandateExpiryDate())>0){
                log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Mandate token has been expired");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }

            //TODO: CREATED RANDOM instrunctionId, refId FOR NOW.. NEED TO CHANGE IT TO MOCCO TRANSACTION ID
            Random rand = new Random(); //instance of random class
            int randomInstructionId = rand.nextInt();
            //TODO: instrunctionId will be mocotransaction id (tid)
            String instrunctionId= String.valueOf(randomInstructionId).replace("-", "");
            //MAKING REFID AND INSTRUCTION ID SAME
            String refId=instrunctionId;


            //GENERATION TOKEN FOR STAGE PAYMENT
            String generateStagePaymentTokenInput= Config.getProperty("cips_participant_id")+","+cipsPaymentRequest.getCips().getMandateToken()+","
                    +userByMandateToken.get("userIdentifier")+","+cipsPaymentRequest.getTxn().getAmount()+","
                    +Config.getProperty("cips_app_id")+","+instrunctionId+","+refId+","+Config.getProperty("cips_username");
            String stagePaymentSignatureToken;
            try {
                stagePaymentSignatureToken=TokenGenerator.generateToken(generateStagePaymentTokenInput);
            } catch (Exception e) {
                log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Failed to generate token for stage payment");
                LOG.error(log.toString());
                response.status(500);
                e.printStackTrace();
                return "GNR_ERR";
            }

            //CREATING STAGE PAYMENT REQUEST BODY
            JSONObject stagePaymentRequestBody=new JSONObject();
            stagePaymentRequestBody.put("participantId", Config.getProperty("cips_participant_id"));
            stagePaymentRequestBody.put("mandateToken", cipsPaymentRequest.getCips().getMandateToken());
            stagePaymentRequestBody.put("amount", cipsPaymentRequest.getTxn().getAmount());
            stagePaymentRequestBody.put("userIdentifier", userByMandateToken.get("userIdentifier"));
            stagePaymentRequestBody.put("appId", Config.getProperty("cips_app_id"));
            stagePaymentRequestBody.put("instructionId", instrunctionId);
            stagePaymentRequestBody.put("refId", refId);
            stagePaymentRequestBody.put("particulars", "");
            stagePaymentRequestBody.put("remarks", cipsPaymentRequest.getTxn().getRemarks());
            stagePaymentRequestBody.put("addnField1", "");
            stagePaymentRequestBody.put("addnField2", "");
            stagePaymentRequestBody.put("token", stagePaymentSignatureToken);
            RequestModel stagePaymentModel=new RequestModel(Config.getProperty("cips_stage_payment_url"), stagePaymentRequestBody);


            //MAKE STAGE PAYMENT API CALL
            JSONObject stagePaymentResponse = null;
            try {
                stagePaymentResponse = CipsHandler.makeRequest(stagePaymentModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if ((stagePaymentResponse.has("responseCode") && stagePaymentResponse.get("responseCode").equals("000"))
                    && (stagePaymentResponse.has("responseMessage") && stagePaymentResponse.get("responseMessage").equals("SUCCESS"))){
                log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.SUCCESS, Log.Section.MAKEPAYMENTREQUEST, "Stage payment successfully done");
                LOG.info(log.toString());

                //COLLECTING DATA TO SAVE TRANSACTION
                saveTransactionJson.put("instructionId", stagePaymentResponse.getString("instructionId"));
                saveTransactionJson.put("remarks", stagePaymentResponse.getString("remarks"));


                //VERIFYING SIGNATURE TOKEN FROM STAGE PAYMENT RESPONSE
                String validateStagePaymentTokenInput = Config.getProperty("cips_participant_id")+","+stagePaymentResponse.get("paymentToken")+","+
                        cipsPaymentRequest.getTxn().getAmount()+","+Config.getProperty("cips_app_id")+","+stagePaymentResponse.get("instructionId")+","+
                        stagePaymentResponse.get("secondaryAuthorizationRequired")+","+stagePaymentResponse.get("responseCode")+","+
                        Config.getProperty("cips_username");
                String stagePaymentResponseToken= stagePaymentResponse.getString("token");
                if (!TokenValidator.validateSignatureToken(validateStagePaymentTokenInput, stagePaymentResponseToken)){
                    log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Stage payment signature token verification failed");
                    LOG.error(log.toString());
                    response.status(500);
                    return "GNR_ERR";
                }
                //GENERATE SIGNATURE TOKEN FOR REQUEST PAYMENT
                String generateRequestPaymentTokenInput = Config.getProperty("cips_participant_id")+","+stagePaymentResponse.get("paymentToken")+","+
                        cipsPaymentRequest.getTxn().getAmount()+","+Config.getProperty("cips_app_id")+","+Config.getProperty("cips_username");
                String requestPaymentSignatureToken = null;
                try {
                    requestPaymentSignatureToken = TokenGenerator.generateToken(generateRequestPaymentTokenInput);
                } catch (Exception e) {
                    log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Fail to generate cancel payment token");
                    LOG.error(log.toString());
                    response.status(500);
                    e.printStackTrace();
                    return "GNR_ERR";
                }
                //CREATING REQUEST PAYMENT REQUEST BODY
                JSONObject requestPaymentRequestBody = new JSONObject();
                requestPaymentRequestBody.put("participantId", Config.getProperty("cips_participant_id"));
                requestPaymentRequestBody.put("paymentToken", stagePaymentResponse.get("paymentToken"));
                requestPaymentRequestBody.put("authorizationToken", "1");
                requestPaymentRequestBody.put("amount", cipsPaymentRequest.getTxn().getAmount());
                requestPaymentRequestBody.put("appId", Config.getProperty("cips_app_id"));
                requestPaymentRequestBody.put("token", requestPaymentSignatureToken);
                RequestModel requestPaymentModel=new RequestModel(Config.getProperty("cips_request_payment_url"), requestPaymentRequestBody);

                //MAKE REQUEST PAYMENT API CALL
                JSONObject requestPaymentResponse = null;
                try {
                    requestPaymentResponse = CipsHandler.makeRequest(requestPaymentModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ((requestPaymentResponse.has("responseCode") && requestPaymentResponse.has("responseMessage")) &&
                        requestPaymentResponse.get("responseCode").equals("000") && requestPaymentResponse.get("responseMessage").equals("SUCCESS")){

                    //VERIFYING REQUEST PAYMENT SIGNATURE TOKEN FROM NCHL
                    String validateRequestPaymentTokenInput = Config.getProperty("cips_participant_id")+","+requestPaymentResponse.get("paymentToken")+","+
                            cipsPaymentRequest.getTxn().getAmount()+","+Config.getProperty("cips_app_id")+","+
                            requestPaymentResponse.get("debitStatus")+","+requestPaymentResponse.get("creditStatus")+","+
                            requestPaymentResponse.get("responseCode")+","+Config.getProperty("cips_username");
                    String requestPaymentResponseToken= requestPaymentResponse.getString("token");
                    if (!TokenValidator.validateSignatureToken(validateRequestPaymentTokenInput, requestPaymentResponseToken)){
                        log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Request payment signature token verification failed");
                        LOG.error(log.toString());
                        response.status(500);
                        return "GNR_ERR";
                    }
                    //COLLECTING DATA TO SAVE TRANSACTION
                    saveTransactionJson.put("txnId", requestPaymentResponse.get("txnId"));

                    //SAVING SUCCESSFUL TRANSACTION DATA
                    boolean saved = CipsTransactionDao.saveTransactionData(saveTransactionJson);
                    if (!saved){
                        log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Failed to store transaction data");
                        LOG.error(log.toString());
                    }
                    JSONObject requestPaymentSuccessResponse =new JSONObject();
                    requestPaymentSuccessResponse.put("tid", instrunctionId);
                    log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.SUCCESS, Log.Section.MAKEPAYMENTREQUEST, "Payment made successfully");
                    LOG.info(log.toString());
                    return requestPaymentSuccessResponse;
                }else {
                    log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Request payment response failed");
                    LOG.error(log.toString());
                    LOG.error("Failed Request Payment Response : "+requestPaymentResponse);
                    response.status(500);
                    return "GRN_ERR";
                }

            }else {
                log=new Log("N/A", userByMandateToken.get("userIdentifier"), Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Stage payment response failed");
                LOG.error(log.toString());
                LOG.error("Failed Stage Payment Response : "+stagePaymentResponse);
                response.status(500);
                return "GNR_ERR";
            }
        }
//------------------------------------------------------------------------------------------------------------------------
        //FOR CANCEL PAYMENT i.e. if txn type in request body is VoidSale
//------------------------------------------------------------------------------------------------------------------------
        else if (cipsPaymentRequest.getTxn().getType().equals("VoidSale")){
            log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.CANCELPAYMENTREQUEST, "Starting payment cancellation process");
            LOG.info(log.toString());
            if (cipsPaymentRequest.getTxn().getTid()==null){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "tid missing in body for cancelling payment");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }
            if (cipsPaymentRequest.getTxn().getTid().equals("")){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "tid missing in body for cancelling payment");
                LOG.error(log.toString());
                response.status(400);
                return "GNR_PARAM_MISSING";
            }

            //PRINTING REQUEST BODY TO CHECK ERRORS IN FUTURE
            LOG.info("Request body : "+cipsPaymentRequest.toString());
            //GET DATA BY TID
            Map<String, String> getByTid = CipsTransactionDao.getByTid(cipsPaymentRequest.getTxn().getTid());
            if (getByTid.size()==0){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "Tid "+cipsPaymentRequest.getTxn().getTid()+" doesnt exists in the system");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }

            //CHECKING TRANSACTION TYPE TO CHECK IT ALREADY CANCELLED OR NOT
            if (getByTid.get("type").equals("VoidSale")){
                log=new Log("N/A", getByTid.get("userIdentifier"), Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "Tid "+cipsPaymentRequest.getTxn().getTid()+" has already been cancelled");
                LOG.error(log.toString());
                response.status(422);
                return "GNR_INVALID_DATA";
            }

            //GENERATING SIGNATURE TOKEN FOR CANCEL PAYMENT
            String cancelPaymentSignatureToken=null;
            String generateCancelPaymentTokenInput= Config.getProperty("cips_app_id")+","+getByTid.get("amount")+","+
                    getByTid.get("txnId")+","+Config.getProperty("cips_username");
            try {
                cancelPaymentSignatureToken = TokenGenerator.generateToken(generateCancelPaymentTokenInput);
            } catch (Exception e) {
                log=new Log("N/A", getByTid.get("userIdentifier"), Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "Fail to generate cancel payment token");
                LOG.error(log.toString());
                response.status(500);
                e.printStackTrace();
                return "GNR_ERR";
            }

            //CREATING REQUEST BODY FOR CANCEL PAYMENT
            JSONObject cancelPaymentJson=new JSONObject();
            cancelPaymentJson.put("appId", Config.getProperty("cips_app_id"));
            cancelPaymentJson.put("amount", getByTid.get("amount"));
            cancelPaymentJson.put("txnId", getByTid.get("txnId"));
            cancelPaymentJson.put("token", cancelPaymentSignatureToken);
            RequestModel cancelPaymentRequestBody =new RequestModel(Config.getProperty("cips_cancel_payment_url"), cancelPaymentJson);

            //MAKING CANCEL PAYMENT API CALL
            JSONObject cancelPaymentResponse=null;
            try {
                cancelPaymentResponse = CipsHandler.makeRequest(cancelPaymentRequestBody);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if ((cancelPaymentResponse.has("responseCode") && cancelPaymentResponse.has("responseMessage")) &&
                    (cancelPaymentResponse.get("responseCode").equals("000") && cancelPaymentResponse.get("responseMessage").equals("SUCCESS"))){
                log=new Log("N/A", getByTid.get("userIdentifier"), Log.Status.SUCCESS, Log.Section.CANCELPAYMENTREQUEST, "Payment cancelled successfully");
                LOG.info(log.toString());

                //UPDATING TYPE IN TRANSACTION TABLE IF SUCCESS
                boolean cancelUpdated = CipsTransactionDao.updateTransactionData(cipsPaymentRequest.getTxn().getType(), cipsPaymentRequest.getTxn().getTid());
                if (!cancelUpdated){
                    log=new Log("N/A", getByTid.get("userIdentifier"), Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "Fail to update cancel payment data");
                    LOG.error(log.toString());
                }
                JSONObject cancelPaymentSuccessResponse = new JSONObject();
                cancelPaymentSuccessResponse.put("tid", getByTid.get("tid"));
                return cancelPaymentSuccessResponse;

            }else {
                log=new Log("N/A", getByTid.get("userIdentifier"), Log.Status.FAILED, Log.Section.CANCELPAYMENTREQUEST, "Fail to cancel payment");
                LOG.error(log.toString());
                response.status(500);
                return "GNR_ERR";
            }
        }
//------------------------------------------------------------------------------------------------------------------------
        //If txn type in request body is invalid
//------------------------------------------------------------------------------------------------------------------------
        else {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.MAKEPAYMENTREQUEST, "Invalid txn type");
            LOG.error(log.toString());
            response.status(422);
            return "GNR_INVALID_DATA";
        }

    }
}
