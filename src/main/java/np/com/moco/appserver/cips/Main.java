package np.com.moco.appserver.cips;

import np.com.moco.appserver.cips.account.CipsAccountController;
import np.com.moco.appserver.cips.transaction.CipsTransactionController;
import np.com.moco.appserver.cips.utils.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import static spark.Spark.*;

public class Main {
    private static final Logger LOG = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        Configurator.initialize(null, "log4j2.properties");
        port(Integer.parseInt(Config.getProperty("server_port")));

        get("/account", ((request, response) -> {
            LOG.info("Calling API => '/account'");
            return CipsAccountController.getInitialData(request, response);
        }));
        post("/account/mandatetoken", ((request, response) -> {
            LOG.info("Calling => '/mandatetoken'");
            return CipsAccountController.saveMandateToken(request, response);
        }));
        get("/account/status", ((request, response) -> {
            LOG.info("Calling => '/account/status'");
            return CipsAccountController.getRedirectUrl(request, response);
        }));
        get("/account/:identifier", ((request, response) -> {
            String identifier = request.params("identifier");
            LOG.info("Calling => '/account/"+identifier+"'");
            return CipsAccountController.getAccountDetail(request, response, identifier);
        }));
        get("/account/image/:bankId", ((request, response) -> {
            String bankId = request.params("bankId");
            LOG.info("Calling => '/account/image/"+bankId+"'");
            return CipsAccountController.getBankImage(request, response, bankId);
        }));
        post("/transaction/cips", ((request, response) -> {
            LOG.info("Calling => '/transaction/cips'");
            return CipsTransactionController.makePaymentRequest(request, response);
        }));

    }
}
