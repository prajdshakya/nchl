package np.com.moco.appserver.cips.transaction;

import np.com.moco.appserver.cips.common.Log;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class CipsHandler {
    private static final Logger LOG = LogManager.getLogger(CipsHandler.class);
    private static Log log;
    protected static JSONObject makeRequest(RequestModel requestModel) throws Exception {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.MAKEREQUEST, "Calling nchl api : "+requestModel.getUrl());
        LOG.info(log.toString());
        LOG.info(requestModel.getMainJson());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .retryOnConnectionFailure(false).build();
        RequestBody body = RequestBody.create(requestModel.getMainJson().toString(), JSON);

        HttpUrl url = HttpUrl.parse(requestModel.getUrl()).newBuilder()
                .build();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer "+CipsAuthoriztionTokenGenerator.getToken())
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute();) {
            String jsonResponse = response.body().string();
            JSONObject jsonResponseObject = new JSONObject(jsonResponse);
            return jsonResponseObject;
        }
    }
}
