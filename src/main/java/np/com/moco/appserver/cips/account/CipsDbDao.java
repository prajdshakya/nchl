package np.com.moco.appserver.cips.account;

import np.com.moco.appserver.cips.common.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import np.com.moco.appserver.cips.utils.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CipsDbDao {
    private static final Logger LOG = LogManager.getLogger(CipsDbDao.class);
    private static Log log;

    public static User getUserDetail(String id){
        String query="SELECT * FROM user WHERE id=?";
        User user=new User();
        log=new Log("N/A", id, Log.Status.PROCESSING, Log.Section.GETUSERDETAIL, "Trying to get user detail of "+id);
        LOG.info(log.toString());
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query);) {
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    user.setId(resultSet.getString("id"));
                    user.setName(resultSet.getString("name"));
                    user.setEmail(resultSet.getString("email"));
                }
        } catch (SQLException sqlException) {
            log=new Log("N/A", id, Log.Status.FAILED, Log.Section.GETUSERDETAIL, "Error while loading user detail"+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", id, Log.Status.FAILED, Log.Section.GETUSERDETAIL, "Error while loading user detail");
            LOG.error(log.toString());
            sqlException.printStackTrace();
        }
        return user;
    }

    public static boolean saveInitialData(Cips cips) {
        boolean result=false;
        String query = "INSERT INTO cips_account(identifier,user_id,device_id,amount,debit_type,frequency,mandate_start_date,mandate_expiry_date,requested_on) VALUES(?,?,?,?,?,?,?,?, current_timestamp())";
        log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.PROCESSING, Log.Section.SAVEINITIALDATA, "Saving initial data");
        LOG.info(log.toString());
        try (Connection connection=DbConnection.getCon();
             PreparedStatement preparedStatement=connection.prepareStatement(query)) {

            preparedStatement.setString(1, cips.getIdentifier());
            preparedStatement.setString(2, cips.getUserIdentifier());
            preparedStatement.setString(3, cips.getDeviceId());
            preparedStatement.setString(4, cips.getAmount());
            preparedStatement.setString(5, cips.getDebitType());
            preparedStatement.setString(6, cips.getFrequency());
            preparedStatement.setString(7, cips.getMandateStartDate());
            preparedStatement.setString(8, cips.getMandateExpiryDate());

            preparedStatement.executeUpdate();
            result=true;
        } catch (SQLException sqlException) {
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEINITIALDATA, "Error while saving initial data "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEINITIALDATA, "Saving initial data");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;
    }

    public static boolean isIdentifierExists(String identifier) {
        boolean found;
        String query = "SELECT * FROM cips_account WHERE identifier=?";
        log=new Log(identifier, "N/A", Log.Status.PROCESSING, Log.Section.GETBYIDENTIFIER, "Trying to get data by identifier");
        LOG.info(log.toString());
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)) {

            preparedStatement.setString(1, identifier);
            ResultSet resultSet = preparedStatement.executeQuery();
            found= resultSet.next();
            return found;
        } catch (SQLException sqlException) {
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETBYIDENTIFIER, "Error while getting data by identifier "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETBYIDENTIFIER, "Error while getting data by identifier");
            LOG.error(log.toString());
            found=false;
            sqlException.printStackTrace();
        }
        return false;
    }

    public static boolean saveMandateToken(Cips cips) {
        boolean result=false;
        String query="UPDATE cips_account set mandate_token=?,mandate_token_type=?,entry_id=?,mandate_token_nickname=?," +
                "bank_id=?,bank_name=? WHERE identifier=?";
        log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.PROCESSING, Log.Section.SAVEMANDATETOKEN, "Trying to save mandate token");
        LOG.info(log.toString());
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)) {

            preparedStatement.setString(1, cips.getMandateToken());
            preparedStatement.setString(2, cips.getMandateTokenType());
            preparedStatement.setString(3, cips.getEntryId());
            preparedStatement.setString(4, cips.getMandateTokenNickName());
            preparedStatement.setString(5, cips.getBankId());
            preparedStatement.setString(6, cips.getBankName());
            preparedStatement.setString(7, cips.getIdentifier());

            preparedStatement.executeUpdate();
            result=true;
        } catch (SQLException sqlException) {
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Error while saving mandate token "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Error while saving mandate token");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;
    }

    public static Cips getCipsDataByIdentifier(String identifier) {
        log=new Log(identifier, "N/A", Log.Status.PROCESSING, Log.Section.GETCIPSDATABYIDENTIFIER, "Trying get full detail of identifier "+identifier);
        LOG.info(log.toString());
        String query = "SELECT * FROM cips_account WHERE identifier=?";
        Cips cips=new Cips();
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)) {

            preparedStatement.setString(1, identifier);
            ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    cips.setIdentifier(resultSet.getString("identifier"));
                    cips.setUserIdentifier(resultSet.getString("user_id"));
                    cips.setDeviceId(resultSet.getString("device_id"));
                    cips.setAmount(resultSet.getString("amount"));
                    cips.setDebitType(resultSet.getString("debit_type"));
                    cips.setFrequency(resultSet.getString("frequency"));
                    cips.setMandateStartDate(resultSet.getString("mandate_start_date"));
                    cips.setMandateExpiryDate(resultSet.getString("mandate_expiry_date"));
                    cips.setMandateToken(resultSet.getString("mandate_token"));
                    cips.setMandateTokenType(resultSet.getString("mandate_token_type"));
                    cips.setEntryId(resultSet.getString("entry_id"));
                    cips.setMandateTokenNickName(resultSet.getString("mandate_token_nickname"));
                    cips.setBankId(resultSet.getString("bank_id"));
                    cips.setBankName(resultSet.getString("bank_name"));
                }
        } catch (SQLException sqlException) {
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.GETCIPSDATABYIDENTIFIER, "Error while getting full detail of identifier "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.GETCIPSDATABYIDENTIFIER, "Error while getting full detail of identifier");
            LOG.error(log.toString());
            sqlException.printStackTrace();
        }
        return cips;
    }

    public static boolean saveUser(User user) {
        log=new Log("N/A", user.getId(), Log.Status.PROCESSING, Log.Section.SAVEUSER, "Trying to save new user");
        LOG.info(log.toString());
        boolean result=false;
        String query="insert into user(id,name,email) values(?,?,?)";
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query);) {

            preparedStatement.setString(1, user.getId());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getEmail());

            preparedStatement.executeUpdate();
            result = true;
        } catch (SQLException sqlException) {
            log=new Log("N/A", user.getId(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Error while saving user "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", user.getId(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Error while saving user");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;
    }
}
