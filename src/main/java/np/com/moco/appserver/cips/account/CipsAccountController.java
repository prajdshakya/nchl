package np.com.moco.appserver.cips.account;

import com.google.gson.Gson;
import np.com.moco.appserver.cips.common.Log;
import np.com.moco.appserver.cips.common.TokenGenerator;
import np.com.moco.appserver.cips.common.TokenValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import np.com.moco.appserver.cips.utils.Config;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CipsAccountController {
    private static final Logger LOG = LogManager.getLogger(CipsAccountController.class);
    private static final Gson gson=new Gson();
    private static Log log;


    public static Object getInitialData(Request request, Response response) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GETINITIALDATA, "Getting Initial Data");
        LOG.info(log.toString());

        response.type("application/json");
        //checking empty X-MOCO-AUTH-TOKEN, X-MOCO-DEVICE
        String authtoken = request.headers("X-MOCO-AUTH-TOKEN");
        String mocoDevice = request.headers("X-MOCO-DEVICE");
        if ((authtoken==null || authtoken=="") || (mocoDevice==null || mocoDevice=="")){
            response.status(400);
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETINITIALDATA, "Missing X-MOCO-AUTH-TOKEN or X-MOCO-DEVICE");
            LOG.error(log.toString());
            return "GNR_PARAM_MISSING";
        }

        //for setting current date into mandateStartDate
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        //for setting mandateExpiryDate 180 day from mandateStartDate: reduced -5 days from 180 days to avoid conflict
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        Format f = new SimpleDateFormat("yyyy-MM-dd");
        date.add(Calendar.MONTH, Integer.parseInt(Config.getProperty("cips_madate_token_validity")));

        //for setting random UUID in identifier
        UUID identifier = UUID.randomUUID();

        Cips cips=new Cips();
        //TODO: need to call user and send mobile as id
        User userDetail = CipsDbDao.getUserDetail("9813527106");
        if (userDetail.getId()==null || userDetail.getId().equals("")){
            User user=new User();
            user.setId("9841192019");
            user.setName("Pramila");
            user.setEmail("example@moco.com.np");
            CipsDbDao.saveUser(user);
            userDetail = CipsDbDao.getUserDetail(user.getId());
        }
        cips.setParticipantId(Config.getProperty("cips_participant_id"));
        cips.setIdentifier(identifier.toString());
        cips.setDeviceId("VIVO V7");
        cips.setMobileNo(userDetail.getId());
        cips.setEmail(userDetail.getEmail());
        cips.setUserIdentifier(userDetail.getId());
        cips.setAmount(Config.getProperty("cips_amount"));
        cips.setDebitType(Config.getProperty("cips_debit_type"));
        cips.setFrequency(Config.getProperty("cips_frequency"));
        cips.setMandateStartDate(dtf.format(now));
        cips.setMandateExpiryDate(f.format(date.getTime()));
        cips.setUrl(Config.getProperty("cips_url"));

        //input combination for generating initial token
        String input = cips.getParticipantId()+","
                +cips.getIdentifier()+","
                +cips.getUserIdentifier()+","
                +cips.getMobileNo()+","
                +cips.getEmail()+","
                +cips.getAmount()+","
                +cips.getDebitType()+","
                +cips.getFrequency()+","
                +cips.getMandateStartDate()+","
                +cips.getMandateExpiryDate();

        //getting initial token
        System.out.println(input);
        String token = null;
        try {
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.PROCESSING, Log.Section.GETINITIALDATA, "Preparing initial login token");
            LOG.info(log.toString());
            token = TokenGenerator.generateToken(input);
        } catch (Exception e) {
            response.status(500);
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.GETINITIALDATA, "Cannot create initial token for user to redirect into login page");
            LOG.error(log.toString());
            e.printStackTrace();
            return "GNR_ERR";
        }
        cips.setToken(token);
        boolean result = CipsDbDao.saveInitialData(cips);
        if (result){
            cips.setDeviceId(null);
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.SUCCESS, Log.Section.GETINITIALDATA, "Initial data send successfully to the user");
            LOG.info(log.toString());
            //TODO: need to send user token later
            response.header("X-MOCO-AUTH-TOKEN", "exampleMocoAuthTokenToken");
            return gson.toJson(cips);
        }else {
            response.status(500);
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.GETINITIALDATA, "failed to load data");
            LOG.error(log.toString());
            return "GNR_ERR";
        }
    }

    public static Object saveMandateToken(Request request, Response response) {

        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.SAVEMANDATETOKEN, "Starting to save mandate token");
        LOG.info(log.toString());

        response.type("application/json");
        JSONObject body=new JSONObject();
        JSONObject data=new JSONObject();
        String [] message;
        Cips cips;
        if (request.body().equals("") || request.body().equals(null)){
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Missing JSON body");
            LOG.error(log.toString());
            message = new String[]{"Missing JSON body"};
            body.put("responseCode", "400");
            body.put("responseMessage", "ERROR");
            body.put("error", message);
            return body;
        }
        cips=new Gson().fromJson(new JSONObject(request.body()).toString(), Cips.class);

        //checking null and empty fields
        if ((cips.getParticipantId()==null ||cips.getMandateToken()==null || cips.getMandateTokenType()==null ||
                cips.getUserIdentifier()==null || cips.getEntryId()==null || cips.getMandateTokenNickName()==null ||
                cips.getBankId()==null || cips.getBankName()==null || cips.getIdentifier()==null || cips.getMobileNo()==null
                || cips.getEmail()==null || cips.getAmount()==null || cips.getFrequency()==null || cips.getMandateStartDate()==null ||
                cips.getMandateExpiryDate()==null || cips.getToken()==null)||
            (cips.getParticipantId().equals("") || cips.getMandateToken().equals("")|| cips.getMandateTokenType().equals("")||
                cips.getEntryId().equals("")|| cips.getMandateTokenNickName().equals("")|| cips.getUserIdentifier()=="" ||
                cips.getBankId().equals("")|| cips.getBankName().equals("")|| cips.getIdentifier().equals("") || cips.getMobileNo().equals("")
                || cips.getEmail().equals("") || cips.getAmount().equals("") || cips.getFrequency().equals("") || cips.getMandateStartDate().equals("") ||
                    cips.getMandateExpiryDate().equals("") || cips.getToken().equals(""))){

            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Invalid JSON or missing JSON attribute");
            LOG.error(log.toString());
            message = new String[]{"Invalid JSON or missing JSON attribute"};
            body.put("responseCode", "422");
            body.put("responseMessage", "ERROR");
            body.put("error", message);
            return body;
        }

        boolean ifIdentifierExists = CipsDbDao.isIdentifierExists(cips.getIdentifier());
        if (!ifIdentifierExists){
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Given identifier not found");
            LOG.error(log.toString());
            message = new String[]{"Given identifier not found"};
            body.put("responseCode", "404");
            body.put("responseMessage", "ERROR");
            body.put("error", message);
            return body;
        }

        //verifying signature token
        String tokenString = cips.getParticipantId()+","+cips.getIdentifier()+","+cips.getUserIdentifier()+
                ","+cips.getMobileNo()+","+cips.getEmail()+","+cips.getAmount()+","+cips.getDebitType()+","+cips.getFrequency()+
                ","+cips.getMandateStartDate()+","+cips.getMandateExpiryDate()+","+cips.getMandateToken()+
                ","+cips.getMandateTokenType();

        if (!TokenValidator.validateSignatureToken(tokenString, cips.getToken())){
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Mandate token verification failed. Invalid mandate token");
            LOG.error(log.toString());
            message = new String[]{"Mandate token verification failed. Invalid mandate token"};
            body.put("responseCode", "410");
            body.put("responseMessage", "ERROR");
            body.put("error", message);
            return body;
        }
        boolean saved = CipsDbDao.saveMandateToken(cips);
        if (saved){
            String input = cips.getIdentifier()+","+cips.getParticipantId()+","+cips.getEntryId();
            String token =null;

            try {
                log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.PROCESSING, Log.Section.SAVEMANDATETOKEN, "Preparing signature token for success response");
                LOG.info(log.toString());
                token = TokenGenerator.generateToken(input);
            } catch (Exception e) {
                log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.SAVEMANDATETOKEN, "Cannot create token for mandate token success response");
                LOG.error(log.toString());
                e.printStackTrace();
                message = new String[]{"Cannot create token for mandate token success response"};
                body.put("responseCode", "410");
                body.put("responseMessage", "ERROR");
                body.put("error", message);
                return body;
            }
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.SUCCESS, Log.Section.SAVEMANDATETOKEN, "Mandate Token save successfully");
            LOG.info(log.toString());
            //success response
            data.put("identifier", cips.getIdentifier());
            data.put("participantId", cips.getParticipantId());
            data.put("entryId", cips.getEntryId());
            data.put("token", token);

            body.put("responseCode", "000");
            body.put("responseMessage", "SUCCESS");
            body.put("data", data);
            body.put("error", new String[0]);

            return body;
        }else {
            log=new Log(cips.getIdentifier(), cips.getUserIdentifier(), Log.Status.FAILED, Log.Section.GETINITIALDATA, "Failed to save data");
            LOG.error(log.toString());
            message = new String[]{"Failed to save data"};
            body.put("responseCode", "500");
            body.put("responseMessage", "ERROR");
            body.put("error", message);
            return body;
        }
    }

    public static Object getRedirectUrl(Request request, Response response) {

        String identifier = request.queryParams("identifier");
        String participantId = request.queryParams("participantId");

        if ((identifier.equals(null) || participantId.equals(null)) || (identifier.equals("") || participantId.equals(""))){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETREDIRECTURL, "Query params are missing");
            LOG.error(log.toString());
            response.status(400);
            return "GNR_PARAM_MISSING";
        }

        if (!participantId.equals(Config.getProperty("cips_participant_id"))){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETREDIRECTURL, "Participant id format verification failed");
            LOG.error(log.toString());
            response.status(422);
            return "GNR_INVALID_DATA";
        }

        try{
            UUID.fromString(identifier);
            log=new Log(identifier, "N/A", Log.Status.SUCCESS, Log.Section.GETREDIRECTURL, "Identifier format verified successfully");
            LOG.info(log.toString());
        } catch (IllegalArgumentException exception){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETREDIRECTURL, "Identifier format verification failed");
            LOG.error(log.toString());
            response.status(422);
            return "GNR_INVALID_DATA";
        }

        log=new Log(identifier, "N/A", Log.Status.PROCESSING, Log.Section.GETREDIRECTURL, "Trying to redirect user");
        LOG.info(log.toString());


        Cips cipsData = CipsDbDao.getCipsDataByIdentifier(identifier);
        if ((cipsData.getIdentifier()==null || cipsData.getMandateToken()==null) || (cipsData.getIdentifier()=="" || cipsData.getMandateToken()=="")){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETREDIRECTURL, "Given identifier or mandate token not found");
            LOG.error(log.toString());
            response.status(404);
            return "GNR_NOT_FOUND";
        }
        log=new Log(identifier, "N/A", Log.Status.SUCCESS, Log.Section.GETREDIRECTURL, "User redirected successfully");
        LOG.info(log.toString());
        return "GNR_OK";
    }

    public static Object getAccountDetail(Request request, Response response, String identifier) {
        log=new Log(identifier, "N/A", Log.Status.PROCESSING, Log.Section.GETACCOUNTDETAIL, "Trying to get account details");
        LOG.info(log.toString());

        response.type("application/json");

        //checking empty X-MOCO-AUTH-TOKEN, X-MOCO-DEVICE
        String authtoken = request.headers("X-MOCO-AUTH-TOKEN");
        String mocoDevice = request.headers("X-MOCO-DEVICE");
        if ((authtoken==null || authtoken=="") || (mocoDevice==null || mocoDevice=="")){
            response.status(400);
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETACCOUNTDETAIL, "Missing X-MOCO-AUTH-TOKEN or X-MOCO-DEVICE");
            LOG.error(log.toString());
            return "GNR_PARAM_MISSING";
        }

        try{
            UUID.fromString(identifier);
            log=new Log(identifier, "N/A", Log.Status.SUCCESS, Log.Section.GETREDIRECTURL, "Identifier format verified successfully");
            LOG.info(log.toString());
        } catch (IllegalArgumentException exception){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETREDIRECTURL, "Identifier format verification failed");
            LOG.error(log.toString());
            response.status(422);
            return "GNR_INVALID_DATA";
        }

        Cips cipsDataByIdentifier = CipsDbDao.getCipsDataByIdentifier(identifier);
        if ((cipsDataByIdentifier.getIdentifier()==null || cipsDataByIdentifier.getMandateToken()==null) || (cipsDataByIdentifier.getIdentifier()=="" || cipsDataByIdentifier.getMandateToken()=="")){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETACCOUNTDETAIL, "Given identifier or mandate token not found");
            LOG.error(log.toString());
            response.status(404);
            return "GNR_NOT_FOUND";
        }
        //TODO: Need logged in user number over here to compare with user_id in cips_account table
        String loggedInUserMobileNo="9813527106";
        if (!loggedInUserMobileNo.equals(cipsDataByIdentifier.getUserIdentifier())){
            log=new Log(identifier, "N/A", Log.Status.FAILED, Log.Section.GETACCOUNTDETAIL, "Given identifier doesnt belongs to this user");
            LOG.error(log.toString());
            response.status(404);
            return "GNR_NOT_FOUND";
        }
        cipsDataByIdentifier.setIdentifier(null);
        cipsDataByIdentifier.setParticipantId(null);
        cipsDataByIdentifier.setUserIdentifier(null);
        cipsDataByIdentifier.setDeviceId(null);
        cipsDataByIdentifier.setAmount(null);
        cipsDataByIdentifier.setDebitType(null);
        cipsDataByIdentifier.setFrequency(null);
        cipsDataByIdentifier.setMandateStartDate(null);
        cipsDataByIdentifier.setMandateTokenType(null);
        cipsDataByIdentifier.setEntryId(null);

        log=new Log(identifier, "N/A", Log.Status.SUCCESS, Log.Section.GETACCOUNTDETAIL, "Account details returned successfully");
        LOG.info(log.toString());
        //TODO: need to send user token later
        response.header("X-MOCO-AUTH-TOKEN", "exampleMocoAuthTokenToken");
        return gson.toJson(cipsDataByIdentifier);
    }

    public static Object getBankImage(Request request, Response response, String bankId) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GETBANKIMAGE, "Trying to get image of bank id "+bankId);
        LOG.info(log.toString());
        response.type("text/plain");

        String authtoken = request.headers("X-MOCO-AUTH-TOKEN");
        String mocoDevice = request.headers("X-MOCO-DEVICE");
        if ((authtoken==null || authtoken=="") || (mocoDevice==null || mocoDevice=="")){
            response.status(400);
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBANKIMAGE, "Missing X-MOCO-AUTH-TOKEN or X-MOCO-DEVICE");
            LOG.error(log.toString());
            return "GNR_PARAM_MISSING";
        }
        Pattern ptrn = Pattern.compile("[0-9]{4,5}");
        Matcher match = ptrn.matcher(bankId);
        if (!(match.find() && match.group().equals(bankId))){
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBANKIMAGE, "Invalid bank id format");
            LOG.error(log.toString());
            response.status(422);
            return "GNR_INVALID_DATA";
        }

        String imageDir = Config.getProperty("cips_banks_images");
        try {
            File dir = new File(imageDir);
            File[] files = dir.listFiles((File dir1, String name) -> name.startsWith(bankId));

            if (files==null){
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBANKIMAGE, "Folder location not found");
                LOG.error(log.toString());
                response.status(404);
                return "GNR_NOT_FOUND";
            }
            if (files.length > 0) {
                File file = files[0];
                String imgName = file.getName();
                String imgType = imgName.substring(imgName.indexOf(".") + 1);   //png or jpeg
                BufferedImage img = ImageIO.read(file);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                if (imgType.equalsIgnoreCase("png")) {
                    ImageIO.write(img, "png", bos);
                    response.type("image/png");
                } else {
                    ImageIO.write(img, "jpeg", bos);
                    response.type("image/jpeg");
                }
                byte[] data = bos.toByteArray();
                log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GETBANKIMAGE, "Bank image of bank id "+bankId+" returned successfully");
                LOG.info(log.toString());
                //TODO: need to send user token later
                response.header("X-MOCO-AUTH-TOKEN", "exampleMocoAuthTokenToken");
                return data;
            } else {
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBANKIMAGE, "Bank image of bank id "+bankId+" does not exits in system.");
                LOG.error(log.toString());
                response.status(404);
                return "GNR_NOT_FOUND";
            }
        } catch (IOException e) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBANKIMAGE, "Bank image of bank id "+bankId+" not found in system.");
            LOG.error(log.toString());
            response.status(404);
            return "GNR_NOT_FOUND";
        }
    }
}
