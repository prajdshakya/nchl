package np.com.moco.appserver.cips.common;


import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import np.com.moco.appserver.cips.utils.Config;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;


public class TokenValidator {
    private static final Logger LOG = LogManager.getLogger(TokenValidator.class);
    private static Log log;

    public static boolean validateSignatureToken(String tokenString, String requestToken) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.VALIDATESIGNATURETOKEN, "Starting to validate token");
        LOG.info(log.toString());
        if (!verifySignature(tokenString, requestToken, Config.getProperty("cips_nchl_public_certificate"))) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.VALIDATESIGNATURETOKEN, "Token verification failed");
            LOG.error(log.toString());
            return false;
        }else {
            log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.VALIDATESIGNATURETOKEN, "Token verified successfully");
            LOG.info(log.toString());
            return true;
        }
    }

    public static boolean verifySignature(String str, String token, String certificateFile) {
        try {
            java.nio.charset.Charset charset = StandardCharsets.UTF_8;
            byte buffer[] = str.getBytes(charset);

            Signature instance = Signature.getInstance("SHA256withRSA");

            instance.initVerify(getPublicKey(certificateFile));
            instance.update(buffer);

            byte[] signature = base64ToByteArray(token);
            return instance.verify(signature);
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }
    public static byte[] base64ToByteArray(String base64String) throws IOException {
        byte[] decoded = Base64.decodeBase64(base64String.getBytes());
        return decoded;
    }
    public static PublicKey getPublicKey(String certificateFile) throws Exception {
        FileInputStream is = new FileInputStream(certificateFile);
        CertificateFactory f = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) f.generateCertificate(is);
        PublicKey pk = certificate.getPublicKey();
        return pk;

    }

}
