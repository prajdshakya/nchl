package np.com.moco.appserver.cips.transaction;

import np.com.moco.appserver.cips.common.Log;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import np.com.moco.appserver.cips.utils.Config;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Map;


public class CipsAuthoriztionTokenGenerator {
    private static final Logger LOG = LogManager.getLogger(CipsAuthoriztionTokenGenerator.class);
    private static Log log;
    private static final OkHttpClient okHttpClient=new OkHttpClient();
    public static Decoder decoder = Base64.getDecoder();
    public static Encoder encoder=Base64.getEncoder();

    public static String getToken() {
        Map<String, String> accessTokenMap = CipsTransactionDao.getAccessToken();
        String accessToken=accessTokenMap.get("access_token");
        String accessTokenExpiryDate=accessTokenMap.get("access_token_expiry_date");

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String currentDateTime=dtf.format(now);

        if (accessToken==null || currentDateTime.compareTo(accessTokenExpiryDate)>0){
            log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GENERATEACCESSTOKEN, "Access token has been expired/null");
            LOG.info(log.toString());
            generateAccessToken();
            accessTokenMap = CipsTransactionDao.getAccessToken();
        }
        //decode access token and return as new string
        byte[] decodedAccessToken = decoder.decode(accessTokenMap.get("access_token"));
        return new String(decodedAccessToken);
    }

    public static String generateRefreshToken(){
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GENERATEREFRESHTOKEN, "Generating new refresh token");
        LOG.info(log.toString());
        String credential = Credentials.basic("moco", "Abcd@123");
        String refreshToken;
        String expTime;
        RequestBody accessTokenRequestBody = new FormBody.Builder()
                .add("username", "MOCO@999")
                .add("password", "123Abcd@123")
                .add("grant_type", "password")
                .build();

        Request request = new Request.Builder()
                .url(Config.getProperty("cips_oauth_token_url"))
                .header("Authorization", credential)
                .post(accessTokenRequestBody)
                .build();

        JSONObject jsonObject=null;
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GENERATEREFRESHTOKEN, "Fail to generate new refresh token");
                LOG.error(log.toString());
                System.out.println("Error with access token");
            }
            String jsonResponse = response.body().string();
            jsonObject = new JSONObject(jsonResponse);
            refreshToken = jsonObject.get("refresh_token").toString();
            //encoding refresh token to insert into data base
            String encodedRefreshToken = encoder.encodeToString(refreshToken.getBytes());

            expTime = jsonObject.get("expires_in").toString();
            CipsTransactionDao.saveNewRefreshToken(encodedRefreshToken, expTime);

            log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GENERATEREFRESHTOKEN, "Refresh token generated successfully");
            LOG.info(log.toString());
            return jsonObject.get("refresh_token").toString();
        } catch (IOException e) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GENERATEREFRESHTOKEN, "Error while generating new refresh token "+e.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GENERATEREFRESHTOKEN, "Error while generating new refresh token");
            LOG.error(log.toString());
            e.printStackTrace();
        }
        return jsonObject.get("refresh_token").toString();

    }

    public static JSONObject generateAccessToken(){
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GENERATEACCESSTOKEN, "Generating new access token");
        LOG.info(log.toString());
        String credential = Credentials.basic("moco", "Abcd@123");
        String accessToken;
        String expTime;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        Map<String, String> refreshTokenMap=new HashMap<>();
        refreshTokenMap = CipsTransactionDao.getRefreshToken();

        String currentDateTime=dtf.format(now);
        String encryptedRefreshToken=refreshTokenMap.get("refresh_token");

        byte[] decodedRefreshToken = new byte[0];
        if (encryptedRefreshToken==null || currentDateTime.compareTo(refreshTokenMap.get("refresh_token_expiry_date"))>0){
            log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GENERATEACCESSTOKEN, "Refresh token has been expired/null");
            LOG.info(log.toString());
            generateRefreshToken();
            refreshTokenMap = CipsTransactionDao.getRefreshToken();

            //decoding refresh token before using it to get access token
            decodedRefreshToken = decoder.decode(refreshTokenMap.get("refresh_token"));
        }else {
            //decoding refresh token before using it to get access token
            decodedRefreshToken = decoder.decode(encryptedRefreshToken);
        }

        RequestBody accessTokenRequestBody = new FormBody.Builder()
                .add("username", "MOCO@999")
                .add("password", "123Abcd@123")
                .add("grant_type", "refresh_token")
                .add("refresh_token", new String(decodedRefreshToken))
                .build();

        Request request = new Request.Builder()
                .url(Config.getProperty("cips_oauth_token_url"))
                .header("Authorization", credential)
                .post(accessTokenRequestBody)
                .build();

        JSONObject jsonResponseObject =null;
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GENERATEACCESSTOKEN, "Fail to generate new access token");
                LOG.error(log.toString());
                System.out.println("Error with access token");
            }
            String jsonResponse = response.body().string();
            jsonResponseObject = new JSONObject(jsonResponse);
            accessToken = jsonResponseObject.get("access_token").toString();
            //encoding access token to insert into database
            String encodedAccessToken = encoder.encodeToString(accessToken.getBytes());

            expTime = jsonResponseObject.get("expires_in").toString();
            CipsTransactionDao.saveNewAccessToken(encodedAccessToken, expTime);
            log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GENERATEACCESSTOKEN, "Access token generated successfully");
            LOG.info(log.toString());
            return jsonResponseObject;
        } catch (IOException e) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GENERATEACCESSTOKEN, "Error while generating access token");
            LOG.error(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GENERATEACCESSTOKEN, "Error while generating access token "+e.getMessage());
            LOG.debug(log.toString());
            e.printStackTrace();
        }
        return jsonResponseObject;
    }

}
