package np.com.moco.appserver.cips.transaction;

import com.google.gson.Gson;

public class CipsPaymentRequest {

    private Cips cips;
    private Merchant merchant;
    private Txn txn;

    public CipsPaymentRequest() {
    }

    public CipsPaymentRequest(Cips cips, Merchant merchant, Txn txn) {
        this.cips = cips;
        this.merchant = merchant;
        this.txn = txn;
    }

    public Cips getCips() {
        return cips;
    }

    public void setCips(Cips cips) {
        this.cips = cips;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Txn getTxn() {
        return txn;
    }

    public void setTxn(Txn txn) {
        this.txn = txn;
    }

    static class Cips{
        private String mandateToken;
        private String mandateExpiryDate;
        private String mPin;

        public String getMandateToken() {
            return mandateToken;
        }
        public void setMandateToken(String mandateToken) {
            this.mandateToken = mandateToken;
        }

        public String getMandateExpiryDate() {
            return mandateExpiryDate;
        }

        public void setMandateExpiryDate(String mandateExpiryDate) {
            this.mandateExpiryDate = mandateExpiryDate;
        }

        public String getmPin() {
            return mPin;
        }

        public void setmPin(String mPin) {
            this.mPin = mPin;
        }
    }
    static class Merchant{
        private String mid;
        private String name;

        public String getMid() {
            return mid;
        }

        public void setMid(String mid) {
            this.mid = mid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    static class Txn {
        private String type;
        private String tid;
        private String amount;
        private String remarks;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTid() {
            return tid;
        }

        public void setTid(String tid) {
            this.tid = tid;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
