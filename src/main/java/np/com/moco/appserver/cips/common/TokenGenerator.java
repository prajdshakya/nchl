package np.com.moco.appserver.cips.common;

import np.com.moco.appserver.cips.utils.Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;
import java.util.Enumeration;

public class TokenGenerator {
    public static String generateToken(String input)throws Exception{
        PrivateKey pvtkey = null;

        // load key from pfx keystore
        Enumeration<String> aliasList;
        String alias;
        String pfxPath = Config.getProperty("cips_moco_private_certificate"), certPassword = Config.getProperty("cips_moco_private_certificate_password");
        File securityFileKeyPair = new File(pfxPath);
        try (InputStream cerFileStream = new FileInputStream(securityFileKeyPair);) {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(cerFileStream, certPassword.toCharArray());
            aliasList = keyStore.aliases();
            while (aliasList.hasMoreElements()) {
                alias = aliasList.nextElement();
                KeyStore.ProtectionParameter entryPassword = new KeyStore.PasswordProtection(certPassword.toCharArray());
                KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, entryPassword);
                pvtkey = privateKeyEntry.getPrivateKey();

                String algorithm = "SHA256withRSA";

                return sign(algorithm, pvtkey, input);
//                String data = "MOCO@999,123,USER123,9840093425,roshan@moco.com.np,100000.0+,V,7,2022-04-29,2022-06-25,npjDST6fX8ZI3MafI/hxuJIDC2zgH/YS57sFs038tSsGEPbiwqKUDdInwZ8XMbf13RRQ0eq/z/DAqtrXPFBToqFDcrINAEh7i2+Tt6lWtHdDptBG5wedUo6eYXB1adDq,F";
//                String token = "Gil5e8MDXqAoHNhgNW/kfM9d4RyvxOP3fVmGS2mWoGBc7l9yMVdAWZjvfegGVz+rDxoS6v0UaLCflsbvlbivcakXiDbhVuKk4RTC+Qkm1Lyro6w0m9ryY5EvCTMapdwY5HPbCZ3MtK9QRyNgnXeRhr/UIWxT1eTW73aqA5wbLZJrd2yk/5o/8Bdh+tbulkg9GNCPgPdGAVxvxudLFTRuEh4pNJ1isSUd1w0aOPDs1FwsuJc4/zek86viygtnbea2yWy3irsxcDLSkCosrpf4NX8WLHgeoOxgDusPYe3ppHfc3IuvwpegxaCKUtktQfwBMPFnFq+XWqEkYAI7ZfAAUw==";
//                System.out.println(verify(data, algorithm, token));
            }
        }
        return null;
    }


    public static String sign(String algorithm, PrivateKey privateKey, String hashBytes) {
        byte[] signatureEncodedBytes = null;
        try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initSign(privateKey);
            signature.update(hashBytes.getBytes(StandardCharsets.UTF_8));
            byte[] signatureBytes = signature.sign();
            signatureEncodedBytes = Base64.getEncoder().encode(signatureBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException ex) {
            System.out.println(ex.getMessage());
        }
        return new String(signatureEncodedBytes);
    }
}
