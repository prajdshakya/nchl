package np.com.moco.appserver.cips.transaction;

import np.com.moco.appserver.cips.common.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import np.com.moco.appserver.cips.utils.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CipsTransactionDao {
    private static final Logger LOG = LogManager.getLogger(CipsTransactionDao.class);
    private static Log log;

    public static boolean saveNewRefreshToken(String encodeRefreshToken, String expTime) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.SAVENEWREFRESHTOKEN, "Trying to save refresh token");
        LOG.info(log.toString());
        boolean result=false;
        String truncateQuery="truncate cips_token";
        String query = "INSERT INTO cips_token(r_token, r_exp_date) values(?,?)";
        try(Connection connection = DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query);
            PreparedStatement truncateStatement=connection.prepareStatement(truncateQuery);){

            truncateStatement.executeUpdate();

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, Integer.parseInt(expTime));
            Format f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String expiryTime = f.format(calendar.getTime());

            preparedStatement.setString(1, encodeRefreshToken);
            preparedStatement.setString(2, expiryTime);
            preparedStatement.executeUpdate();
            log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.SAVENEWREFRESHTOKEN, "Refresh token saved successfully");
            LOG.info(log.toString());
            result=true;

        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.SAVENEWREFRESHTOKEN, "Error while saving new refresh token "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.SAVENEWREFRESHTOKEN, "Error while saving new refresh token");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;
    }

    public static Map<String, String> getRefreshToken(){
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GETREFRESHTOKEN, "Trying to get refresh token");
        LOG.info(log.toString());
        String query = "Select r_token, r_exp_date from cips_token ORDER BY timestamp DESC LIMIT 1";
        Map<String, String> tokenMapper=new HashMap<>();
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)){

            ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    tokenMapper.put("refresh_token", resultSet.getString("r_token"));
                    tokenMapper.put("refresh_token_expiry_date", resultSet.getString("r_exp_date"));
                    log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GETREFRESHTOKEN, "Refresh token returned successfully");
                    LOG.info(log.toString());
                }
        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETREFRESHTOKEN, "Error while getting refresh token data "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETREFRESHTOKEN, "Error while getting refresh token data");
            LOG.error(log.toString());
            sqlException.printStackTrace();
        }
        return tokenMapper;
    }

    public static boolean saveNewAccessToken(String encodedAccessToken, String expTime) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.SAVENEWACCESSTOKEN, "Trying to save new access token");
        LOG.info(log.toString());
        boolean result=false;
        String query = "UPDATE cips_token SET a_token=?, a_exp_date=?";
        try(Connection connection = DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)){

            Calendar calendar = Calendar.getInstance();
            //TODO: change "10" to expTime from parameter, currently making it expire in 10sec
            calendar.add(Calendar.SECOND, Integer.parseInt("10"));
            Format f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String expiryTime = f.format(calendar.getTime());

            preparedStatement.setString(1, encodedAccessToken);
            preparedStatement.setString(2, expiryTime);
            preparedStatement.executeUpdate();
            log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.SAVENEWACCESSTOKEN, "New access token save successfully");
            LOG.info(log.toString());
            result=true;
        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.SAVENEWACCESSTOKEN, "Error while saving new access token "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.SAVENEWACCESSTOKEN, "Error while saving new access token");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;
    }

    public static Map<String, String> getAccessToken() {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GETACCESSTOKEN, "Trying to get access token");
        LOG.info(log.toString());
        String query = "Select a_token, a_exp_date from cips_token ORDER BY timestamp DESC LIMIT 1";
        Map<String, String> tokenMapper=new HashMap<>();
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)) {

            ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    tokenMapper.put("access_token", resultSet.getString("a_token"));
                    tokenMapper.put("access_token_expiry_date", resultSet.getString("a_exp_date"));
                    log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GETACCESSTOKEN, "Access token returned successfully");
                    LOG.info(log.toString());
                }
        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETACCESSTOKEN, "Error while getting access token "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETACCESSTOKEN, "Error while getting access token");
            LOG.error(log.toString());
            sqlException.printStackTrace();
        }
        return tokenMapper;
    }

    public static Map<String, String> getByMandateToken(String mandateToken) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GETBYMANDATETOKEN, "Trying to get data by mandate token");
        LOG.info(log.toString());
        Map<String, String> cipsAccountDataMapper= new HashMap<>();
        String query = "SELECT * FROM cips_account WHERE mandate_token=?";
        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)) {

            preparedStatement.setString(1, mandateToken);
            ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    cipsAccountDataMapper.put("userIdentifier", resultSet.getString("user_id"));
                    cipsAccountDataMapper.put("mandateExpiryDate", resultSet.getString("mandate_expiry_date"));
                    log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GETBYMANDATETOKEN, "Mandate token data found");
                    LOG.info(log.toString());
                }

        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBYMANDATETOKEN, "Error while getting data from mandate token "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBYMANDATETOKEN, "Error while getting data from mandate token");
            LOG.error(log.toString());
            sqlException.printStackTrace();
        }
        return cipsAccountDataMapper;
    }

    public static boolean saveTransactionData(JSONObject saveTransactionJson) {
        log=new Log("N/A", saveTransactionJson.getString("userIdentifier"), Log.Status.PROCESSING, Log.Section.SAVETRANSACTIONDATA, "Trying to save transaction data");
        LOG.info(log.toString());
        boolean result=false;
        String query="INSERT INTO cips_transaction(tid,user_id,amount,mid,name,remarks,type,nchl_txn_id) values(?,?,?,?,?,?,?,?)";

        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)) {
            preparedStatement.setString(1, saveTransactionJson.getString("instructionId"));
            preparedStatement.setString(2, saveTransactionJson.getString("userIdentifier"));
            preparedStatement.setString(3, saveTransactionJson.getString("amount"));
            preparedStatement.setString(4, saveTransactionJson.getString("mid"));
            preparedStatement.setString(5, saveTransactionJson.getString("name"));
            preparedStatement.setString(6, saveTransactionJson.getString("remarks"));
            preparedStatement.setString(7, saveTransactionJson.getString("type"));
            preparedStatement.setString(8, String.valueOf(saveTransactionJson.getInt("txnId")));
            preparedStatement.executeUpdate();
            log=new Log("N/A", saveTransactionJson.getString("userIdentifier"), Log.Status.SUCCESS, Log.Section.SAVETRANSACTIONDATA, "Transaction saved successfully");
            LOG.info(log.toString());
            result=true;

        } catch (SQLException sqlException) {
            log=new Log("N/A", saveTransactionJson.getString("userIdentifier"), Log.Status.FAILED, Log.Section.SAVETRANSACTIONDATA, "Error while saving transaction data "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", saveTransactionJson.getString("userIdentifier"), Log.Status.FAILED, Log.Section.SAVETRANSACTIONDATA, "Error while saving transaction data");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;

    }

    public static Map<String, String> getByTid(String tid) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.GETBYTID, "Trying to get data by tid "+tid);
        LOG.info(log.toString());
        Map<String, String> transactionDataMapper= new HashMap<>();
        String query = "SELECT * FROM cips_transaction WHERE tid=?";

        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query)){
            preparedStatement.setString(1, tid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                transactionDataMapper.put("tid", resultSet.getString("tid"));
                transactionDataMapper.put("userIdentifier", resultSet.getString("user_id"));
                transactionDataMapper.put("amount", resultSet.getString("amount"));
                transactionDataMapper.put("mid", resultSet.getString("mid"));
                transactionDataMapper.put("name", resultSet.getString("name"));
                transactionDataMapper.put("remarks", resultSet.getString("remarks"));
                transactionDataMapper.put("type", resultSet.getString("type"));
                transactionDataMapper.put("txnId", resultSet.getString("nchl_txn_id"));
                log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.GETBYTID, "Data found for tid: "+tid);
                LOG.info(log.toString());
            }

        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBYTID, "Error while getting data from tid: "+tid+"\n"+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.GETBYTID, "Error while getting data from tid "+tid);
            LOG.error(log.toString());
            sqlException.printStackTrace();
        }
        return transactionDataMapper;
    }

    public static boolean updateTransactionData(String type, String tid) {
        log=new Log("N/A", "N/A", Log.Status.PROCESSING, Log.Section.UPDATETRANSACTIONDATA, "Trying to update type for cancelled data");
        LOG.info(log.toString());
        boolean result=false;
        String query = "UPDATE cips_transaction SET type=? where tid=?";

        try(Connection connection=DbConnection.getCon();
            PreparedStatement preparedStatement=connection.prepareStatement(query);) {
            preparedStatement.setString(1, type);
            preparedStatement.setString(2, tid);
            preparedStatement.executeUpdate();
            result=true;
            log=new Log("N/A", "N/A", Log.Status.SUCCESS, Log.Section.UPDATETRANSACTIONDATA, "Cancelled transaction updated successfully");
            LOG.info(log.toString());
        } catch (SQLException sqlException) {
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.UPDATETRANSACTIONDATA, "Error while updating cancelled transaction "+sqlException.getMessage());
            LOG.debug(log.toString());
            log=new Log("N/A", "N/A", Log.Status.FAILED, Log.Section.UPDATETRANSACTIONDATA, "Error while updating cancelled transaction ");
            LOG.error(log.toString());
            result=false;
            sqlException.printStackTrace();
        }
        return result;

    }
}
