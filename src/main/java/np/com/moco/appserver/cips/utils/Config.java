package np.com.moco.appserver.cips.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
    private static final String CONFIG_FILE = "config.properties";
    private static Properties prop;

    public static void loadConfiguration(){
        //TODO: Create Log class
        //Log log;
        try {
            InputStream inputStream=new FileInputStream(CONFIG_FILE);
            prop = new Properties();
            prop.load(inputStream);
        } catch (IOException e) {
            e.getMessage();
        }
    }

    public static Properties getSectionProperties(String section) {
        if (prop==null || prop.isEmpty()){
            loadConfiguration();
        }
        final Properties p = new Properties();
        prop.forEach((key, value) -> {
            if (key.toString().contains(section)){
                p.put(key, value);
            }
        });
        return p;
    }
    public static String getProperty(String key) {
        if (prop == null || prop.isEmpty()) {
            loadConfiguration();
        }

        return prop.getProperty(key);
    }
}