package np.com.moco.appserver.cips.account;

import java.util.Map;

public class Cips {
    private String participantId;
    private String identifier;
    private String userIdentifier;
    private String deviceId;
    private String mobileNo;
    private String email;
    private String amount;
    private String debitType;
    private String frequency;
    private String mandateStartDate;
    private String mandateExpiryDate;
    private String mandateToken;
    private String mandateTokenType;
    private String entryId;
    private String mandateTokenNickName;
    private String token;
    private String bankId;
    private String bankName;
    private String url;


    public Cips() {
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDebitType() {
        return debitType;
    }

    public void setDebitType(String debitType) {
        this.debitType = debitType;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getMandateStartDate() {
        return mandateStartDate;
    }

    public void setMandateStartDate(String mandateStartDate) {
        this.mandateStartDate = mandateStartDate;
    }

    public String getMandateExpiryDate() {
        return mandateExpiryDate;
    }

    public void setMandateExpiryDate(String mandateExpiryDate) {
        this.mandateExpiryDate = mandateExpiryDate;
    }

    public String getMandateToken() {
        return mandateToken;
    }

    public void setMandateToken(String mandateToken) {
        this.mandateToken = mandateToken;
    }

    public String getMandateTokenType() {
        return mandateTokenType;
    }

    public void setMandateTokenType(String mandateTokenType) {
        this.mandateTokenType = mandateTokenType;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getMandateTokenNickName() {
        return mandateTokenNickName;
    }

    public void setMandateTokenNickName(String mandateTokenNickName) {
        this.mandateTokenNickName = mandateTokenNickName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }




    //====================================
    private Map<String, Object> cips;
    private Map<String, Object> merchant;
    private Map<String, Object> txn;

    public Map<String, Object> getCips() {
        return cips;
    }

    public void setCips(Map<String, Object> cips) {
        this.cips = cips;
    }

    public Map<String, Object> getMerchant() {
        return merchant;
    }

    public void setMerchant(Map<String, Object> merchant) {
        this.merchant = merchant;
    }

    public Map<String, Object> getTxn() {
        return txn;
    }

    public void setTxn(Map<String, Object> txn) {
        this.txn = txn;
    }
}
