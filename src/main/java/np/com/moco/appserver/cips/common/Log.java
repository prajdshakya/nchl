package np.com.moco.appserver.cips.common;

import com.google.gson.Gson;

public class Log {

    private String identifier;
    private String userIdentifier;
    private Status status;
    private Section section;
    private String description;

    public enum Status {
        SUCCESS, FAILED, PROCESSING
    }

    public enum Section {
        GETINITIALDATA, SAVEINITIALDATA, SAVEMANDATETOKEN, GETUSERDETAIL, GETBYIDENTIFIER, VALIDATESIGNATURETOKEN, GETREDIRECTURL,
        GETCIPSDATABYIDENTIFIER, GETACCOUNTDETAIL, GETBANKIMAGE, SAVEUSER, MAKEPAYMENTREQUEST, SAVENEWREFRESHTOKEN, GETREFRESHTOKEN,
        SAVENEWACCESSTOKEN, GETACCESSTOKEN, GETBYMANDATETOKEN, SAVETRANSACTIONDATA, MAKEREQUEST, CANCELPAYMENTREQUEST, GETBYTID,
        UPDATETRANSACTIONDATA, GETTOKEN, GENERATEACCESSTOKEN, GENERATEREFRESHTOKEN
    }

    public Log(String identifier, String userIdentifier, Status status, Section section, String description) {
        this.identifier = identifier;
        this.userIdentifier = userIdentifier;
        this.status = status;
        this.section = section;
        this.description = description;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
