@ignore
Feature: Connect Ips Request Payment Test Cases

  Background:
    * url cipsBaseUrl
    * def accessToken = access_token

            #Note that access token is not sent in header, so it should produce error
  Scenario: Make request payment without Authorization header
    Given path '/tokenization/requestpayment'
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 401
    Then assert responseStatus == expectedStatus
    And match $ == {"error": "unauthorized","error_description": "Full authentication is required to access this resource"}

        #Note that access token used in header 'Bearer 80d017b2-6c18-47c6-9fde-4990d06ff7e9' has be expired, so it should produce error
  Scenario: Calling request payment with expired/invalid access token
    Given path '/tokenization/requestpayment'
    And header Authorization = 'Bearer 80d017b2-6c18-47c6-9fde-4990d06ff7e9'
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 401
    And match $ == {"error": "invalid_token","error_description": "Invalid access token: 80d017b2-6c18-47c6-9fde-4990d06ff7e9"}

            #Note that request body is not set while calling api, NCHL return with blank response
  Scenario: Calling request payment without request body
      Given path '/tokenization/requestpayment'
      And header Authorization = accessToken
      When method POST
      And print response
      * def expectedStatus = 400

            #Note that required field in requestPaymentRequestBodyWithRequiredFieldsEmpty empty, NCHL return with error response
    Scenario: Calling request payment with required fields empty
      Given path '/tokenization/requestpayment'
      And header Authorization = accessToken
      And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBodyWithRequiredFieldsEmpty.json')
      * print BodyOfRequest
      And request BodyOfRequest
      * print BodyOfRequest
      When method POST
      And print response
      * def expectedStatus = 400
      Then assert responseStatus == expectedStatus
      * def expectedResponse = read('jsons/negative/requestPayment/requestPaymentResponseWithAllRequiredFieldsEmpty.json')
      And match $ == expectedResponse
      * print expectedResponse

            #Note that participantId in requestPaymentRequestBodyWithParticipantIdEmpty is empty, NCHL return with error response
  Scenario: Calling request payment with participantId empty
    Given path '/tokenization/requestpayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBodyWithParticipantIdEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/requestPayment/requestPaymentResponseWithMissingField.json')
    And match $ == expectedResponse
    * print expectedResponse

              #Note that paymentToken in requestPaymentRequestBodyWithPaymentTokenEmpty is empty, NCHL return with error response
  Scenario: Calling request payment with payment token empty
    Given path '/tokenization/requestpayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBodyWithPaymentTokenEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/requestPayment/requestPaymentResponseWithMissingField.json')
    And match $ == expectedResponse
    * print expectedResponse

                 #Note that token in requestPaymentRequestBodyWithTokenEmpty is empty, NCHL return with error response
  Scenario: Calling request payment with token empty
    Given path '/tokenization/requestpayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBodyWithTokenEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/requestPayment/requestPaymentResponseWithMissingField.json')
    And match $ == expectedResponse
    * print expectedResponse

     #Note that payment token in requestPaymentRequestBodyWithInvalidPaymentToken is invalid, NCHL return with error response
  Scenario: Calling request payment with invalid payment token
    Given path '/tokenization/requestpayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBodyWithInvalidPaymentToken.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    And match $ == {"responseCode":"E003","responseMessage":"INVALID TOKEN","data":"","classfielderrorlist":[]}

         #Note that token in requestPaymentRequestBodyWithInvalidToken is invalid, NCHL return with error response
  Scenario: Calling request payment with invalid token
    Given path '/tokenization/requestpayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentRequestBodyWithInvalidToken.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    And match $ == {"responseCode":"E003","responseMessage":"INVALID TOKEN","data":"","classfielderrorlist":[]}

    Scenario: Calling request payment with expired OTP in Request body
      Given path '/tokenization/requestpayment'
      And header Authorization = accessToken
      And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentExpiredOTPRequestBody.json')
      * print BodyOfRequest
      And request BodyOfRequest
      * print BodyOfRequest
      When method POST
      And print response
      * def expectedStatus = 200
      Then assert responseStatus == expectedStatus
      * def expectedResponse = read('jsons/negative/requestPayment/requestPaymentExpiredOTPResponse.json')
      And match $ == expectedResponse
      * print expectedResponse


    Scenario: Calling request payment with empty OTP in Request body
      Given path '/tokenization/requestpayment'
      And header Authorization = accessToken
      And def BodyOfRequest = read('jsons/negative/requestPayment/requestPaymentEmptyOTPRequestBody.json')
      * print BodyOfRequest
      And request BodyOfRequest
      * print BodyOfRequest
      When method POST
      And print response
      * def expectedStatus = 200
      Then assert responseStatus == expectedStatus
      * def expectedResponse = read('jsons/negative/requestPayment/requestPaymentEmptyOTPResponse.json')
      And match $ == expectedResponse
      * print expectedResponse




