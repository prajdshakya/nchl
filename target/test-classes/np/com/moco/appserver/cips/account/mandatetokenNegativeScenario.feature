#@ignore
Feature: Connect Ips Save Mandate Token Negative Test Cases

  Background:
    * url baseUrl
    * def xmt = xmocotoken
    * def xmd = xmocodevice


     #NOTE that request body is not send
  Scenario: Save mandate token without request body
    Given path '/account/mandatetoken'
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/mandateToken/requestBodyMissingResponse.json')
    And match $ == expectedResponse
    * print expectedResponse


    #NOTE that invalid key value "example": "To make request body invalid" is in request body to make it invalid
  Scenario: Save mandate token with invalid request body
    Given path '/account/mandatetoken'
    And def BodyOfRequest = read('jsons/negative/mandateToken/invalidRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/mandateToken/invalidRequestBodyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #NOTE that some of the required field like identifier,mandateToken,mandateTokenType,entryId,mandateTokenNickName,
    #bankId,bankName are missing in request body
  Scenario: Save mandate token with required fields empty
    Given path '/account/mandatetoken'
    And def BodyOfRequest = read('jsons/negative/mandateToken/requestBodyWithEmptyFields.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/mandateToken/invalidRequestBodyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #NOTE that identifier in request body is invalid and doesnt exist in our system
  Scenario: Save mandate token with invalid identifier
    Given path '/account/mandatetoken'
    And def BodyOfRequest = read('jsons/negative/mandateToken/invalidIdentifierRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/mandateToken/invalidIdentifierResponse.json')
    And match $ == expectedResponse
    * print expectedResponse


    #NOTE that invalid mandate token is send in request body
  Scenario: Mandate token verification fail while saving
    Given path '/account/mandatetoken'
    And def BodyOfRequest = read('jsons/negative/mandateToken/invalidMandateTokenRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/mandateToken/invalidMandateTokenResponse.json')
    And match $ == expectedResponse
    * print expectedResponse