#@ignore
Feature: Connect Ips Initial Data Negative Test Cases

  Background:
    * url baseUrl
    * def xmt = xmocotoken
    * def xmd = xmocodevice

    #NOTE that X-MOCO-AUTH-TOKEN, X-MOCO-DEVICE has not been send in header
  Scenario: Get initial data for login page without X-MOCO-AUTH-TOKEN, X-MOCO-DEVICE
    Given path '/account'
    When method GET
    And print response
    * def expectedStatus = 400
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_PARAM_MISSING'

