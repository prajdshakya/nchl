@ignore
Feature: Connect Ips Cancel Payment Test Cases

  Background:
    * url cipsBaseUrl
    * def accessToken = access_token


         #Note that stage payment was made with amount 10.00 but during cancellation it was changed to 100.00 and generate token
  Scenario: Calling cancel payment without Authorization header
    Given path '/api/v1/transactions/cancel'
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithAmountDifferentThanStagePayment.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 401
    Then assert responseStatus == expectedStatus
    And match $ == {"error":"unauthorized","error_description":"Full authentication is required to access this resource"}

     #Note that access token send here is expired
  Scenario: Calling cancel payment with expired Authorization access token
    Given path '/api/v1/transactions/cancel'
    And header Authorization = 'Bearer 34973fff-6bbe-47a2-8b9d-f32892a2aea6'
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithAmountDifferentThanStagePayment.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 401
    Then assert responseStatus == expectedStatus
    And match $ == {"error":"invalid_token","error_description":"Invalid access token: 34973fff-6bbe-47a2-8b9d-f32892a2aea6"}

     #Note that request body is not send
  Scenario: Calling cancel payment without request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus

    #Note that request body used here has all the fields empty
  Scenario: Calling cancel payment with all fields empty in request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithAllFieldsEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/cancelPayment/cancelPaymentAllFieldsEmptyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #Note that request body used here has appId fields empty
  Scenario: Calling cancel payment with empty appId in request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithAppIdEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/cancelPayment/cancelPaymentEmptyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse


    #Note that request body used here has amount fields empty
  Scenario: Calling cancel payment with empty amount in request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithAmountEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/cancelPayment/cancelPaymentEmptyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

     #Note that request body used here has txnId fields empty
  Scenario: Calling cancel payment with empty txnId in request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithTxnIdEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/cancelPayment/cancelPaymentEmptyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #Note that request body used here has token fields empty
  Scenario: Calling cancel payment with empty token in request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithTokenEmpty.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/cancelPayment/cancelPaymentEmptyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse


    #Note token in request body is invalid, NCHL should through error
  Scenario: Calling cancel payment with invalid token in request body
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithInvalidToken.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    And match $ == {"responseCode":"E003","responseMessage":"INVALID TOKEN","data":"","classfielderrorlist":[]}


     #Note that stage payment was made with amount 10.00 but during cancellation it was changed to 100.00 and generate token
  Scenario: Calling cancel payment with amount different than stage payment
    Given path '/api/v1/transactions/cancel'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/cancelPayment/cancelPaymentWithAmountDifferentThanStagePayment.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 404
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/cancelPayment/cancelPaymentWithAmountDifferentThanStagePaymentResponse.json')
    And match $ == expectedResponse
    * print expectedResponse