@ignore
Feature: Connect Ips Stage Payment Test Cases

  Background:
    * url cipsBaseUrl
    * def accessToken = access_token

  Scenario: Make stage payment success
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/positive/stagePaymentRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 200
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/positive/stagePaymentSuccessResponse.json')
    And match $ == expectedResponse
    * print expectedResponse


  #Note that accessToken is not send in the header section, to produce this error
  Scenario: Calling stage payment without access token
    Given path '/tokenization/stagepayment'
    And def BodyOfRequest = read('jsons/positive/stagePaymentRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 401
    And match $ == {"error": "unauthorized","error_description": "Full authentication is required to access this resource"}

    #Note that access token used in header 'Bearer 80d017b2-6c18-47c6-9fde-4990d06ff7e9' has be expired, so it should produce error
  Scenario: Calling stage payment with expired/invalid access token
    Given path '/tokenization/stagepayment'
    And header Authorization = 'Bearer 80d017b2-6c18-47c6-9fde-4990d06ff7e9'
    And def BodyOfRequest = read('jsons/positive/stagePaymentRequestBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 401
    And match $ == {"error": "invalid_token","error_description": "Invalid access token: 80d017b2-6c18-47c6-9fde-4990d06ff7e9"}


    #Note that request body has not the send, NCHL responds with status code 400 and empty response body
  Scenario: Calling stage payment without request body
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus


    #Note that in stagePaymentRequiredFieldEmptyBody.json some the required fields are empty, so NCHL should
    # respond with error
  Scenario: Calling stage payment with required fields empty in request body
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/stagePayment/stagePaymentRequiredFieldEmptyBody.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/stagePayment/stagePaymentRequiredFieldEmptyResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #Note that in stagePaymentRequestBodyWithEmptyParticipantId.json participantId is empty, so NCHL should
    # respond with error
  Scenario: Calling stage payment with participantId empty
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/stagePayment/stagePaymentRequestBodyWithEmptyParticipantId.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/stagePayment/stagePaymentWithEmptyFieldsResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #Note that in stagePaymentRequestBodyWithEmptyMandateToken.json mandateToken is empty, so NCHL should
    # respond with error
  Scenario: Calling stage payment with mandateToken empty
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/stagePayment/stagePaymentRequestBodyWithEmptyMandateToken.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    * def expectedResponse = read('jsons/negative/stagePayment/stagePaymentWithEmptyFieldsResponse.json')
    And match $ == expectedResponse
    * print expectedResponse

    #Note that in stagePaymentRequestBodyWithEmptyMandateToken.json mandateToken is invalid, so NCHL should
    # respond with error
  Scenario: Calling stage payment with invalid mandate token empty
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/stagePayment/stagePaymentRequestBodyWithInvalidMandateToken.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    And match $ == {"responseCode":"E003","responseMessage":"INVALID TOKEN","data":"","classfielderrorlist":[]}

    #Note that in stagePaymentRequestBodyWithEmptyMandateToken.json token is invalid, so NCHL should
    # respond with error
  Scenario: Calling stage payment with invalid token empty
    Given path '/tokenization/stagepayment'
    And header Authorization = accessToken
    And def BodyOfRequest = read('jsons/negative/stagePayment/stagePaymentRequestBodyWithInvalidToken.json')
    * print BodyOfRequest
    And request BodyOfRequest
    * print BodyOfRequest
    When method POST
    And print response
    * def expectedStatus = 400
    Then assert responseStatus == expectedStatus
    And match $ == {"responseCode":"E003","responseMessage":"INVALID TOKEN","data":"","classfielderrorlist":[]}
