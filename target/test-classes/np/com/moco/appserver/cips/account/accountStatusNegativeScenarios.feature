#@ignore
Feature: Connect Ips Account Status Negative Test Cases

  Background:
    * url baseUrl
    * def xmt = xmocotoken
    * def xmd = xmocodevice

    #NOTE that participantId and identifier in param are blank
  Scenario: Get account status with empty participantId and identifier
    Given path '/account/status'
    And  param participantId = ''
    And  param identifier = ''
    When method GET
    And print response
    * def expectedStatus = 400
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_PARAM_MISSING'

    #NOTE that participantId in param must be same as in config file, here invalid participantId is used to produce the error
  Scenario: Get account status with participantId not matching with config file
    Given path '/account/status'
    And  param participantId = '999@MOCO'
    And  param identifier = '00e861ac-a3b9-47dd-83aa-535e034f723e'
    When method GET
    And print response
    * def expectedStatus = 422
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_INVALID_DATA'

    #NOTE that identifier must be valid UUID otherwise it must throw error
  Scenario: Get account status with invalid UUID format identifier
    Given path '/account/status'
    And  param participantId = 'MOCO@999'
    And  param identifier = '00e861ac'
    When method GET
    And print response
    * def expectedStatus = 422
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_INVALID_DATA'

      #NOTE that identifier '00e861ac-a3b9-47dd-83aa-535e034f723eff' doesnt exist and has to produce error
  Scenario: Get account status with identifier that does not exist in the system
    Given path '/account/status'
    And  param participantId = 'MOCO@999'
    And  param identifier = '00e861ac-a3b9-47dd-83aa-535e034f723eff'
    When method GET
    And print response
    * def expectedStatus = 404
    And print responseStatus
    Then assert responseStatus == expectedStatus
    And match $ == 'GNR_NOT_FOUND'